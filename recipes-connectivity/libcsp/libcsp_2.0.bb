SUMMARY = "CubeSat Space Protocol library"
DESCRIPTION = "Small protocol stack designed to be used \
               with clusters of embedded devices. \
                                                    \
               You can use the EXTRA_OEMESON variable to configure the build system. \
               All the possible configurations are defined in meson_options.txt in the libcsp repository. \
               The way to configure it is to create a .bbappend file and put for instance \
               EXTRA_OEMESON = "-Denable_python3_bindings=true" in it. \
               "

HOMEPAGE = "https://www.libcsp.org"
SECTION = "libs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2915dc85ab8fd26629e560d023ef175c"
PV = "2.0"
SRC_URI = "git://github.com/libcsp/libcsp.git;branch=libcsp-2-0;protocol=https"
SRCREV = "ac58ff917e650ef70eb405a7f598fcc9e4c45cf9"
S = "${WORKDIR}/git"

inherit meson

do_install:append() {
    install -d ${D}${libdir}
    install -m 644 ${WORKDIR}/build/libcsp.so ${D}${libdir}/
    install -d ${D}${includedir}
    install -m 744 ${D}${prefix}/csp/csp_autoconfig.h ${D}${includedir}/
}

PACKAGES = "${PN} ${PN}-dev ${PN}-dbg"

FILES:${PN} += "${libdir} ${prefix}/csp"
FILES:${PN}-dbg += "${libdir}/.debug/"
FILES:${PN}-dev += "${includedir}"
