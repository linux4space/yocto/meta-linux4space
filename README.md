# meta-linux4space

[![pipeline status](https://gitlab.com/linux4space/yocto/meta-linux4space/badges/main/pipeline.svg)](https://gitlab.com/linux4space/yocto/meta-linux4space/-/commits/main)
[![Latest Release](https://gitlab.com/linux4space/yocto/meta-linux4space/-/badges/release.svg)](https://gitlab.com/linux4space/yocto/meta-linux4space/-/releases)

Linux4Space is a collaborative open source project founded with the intention 
of creating an open source Yocto based Linux distribution suitable for space applications. 
The project brings together all the stakeholders: the software and hardware 
developers, the suppliers, and technology companies. Linux4Space is designed to 
be compliant with ESA (European Space Agency) Standards and it is based on 
community defined requirements.

This repo contains the meta-linux4space layer which is the base of the 
distribution. For more specific aplications see 
[meta-linux4space-apps](https://gitlab.com/linux4space/yocto/meta-linux4space-apps).

# License

All Linux4Space projects follow the license of the metadata in the [Yocto project](https://yoctoproject.com).
For a copy of the license document see `COPYING.MIT`.

# Dependencies

 * Yocto 5.0  
   * URI: <https://git.yoctoproject.org/poky>
   * branch: scarthgap

# Patches

Please submit any patches to the meta-linux4space layer repo located at 
<https://gitlab.com/linux4space/yocto/meta-linux4space>.

Maintainers: 
 * Lukáš Mázl (<lukas.mazl@tul.cz>)
 * Lenka Kosková Třísková (<lenka.koskova.triskova@tul.cz>)


# Using the meta-linux4space layer

 1. Get Yocto 5.0.

    `$ git clone -b scarthgap https://git.yoctoproject.org/poky`

 2. Get meta-linux4space. **Note:** you should run this command in the same directory as the previous.

    `$ git clone https://gitlab.com/linux4space/yocto/meta-linux4space`

 3. Initialize yocto development environment.

    `$ source poky/oe-init-build-env`

 4. Add the meta-linux4space layer.

    `$ bitbake-layers add-layer meta-linux4space`

# Misc

Website: <https://linux4space.org>  
Project wiki: <https://wiki.linux4space.org>  
Mailing list: <https://list.tul.cz/mailman/listinfo/linux4space>  
